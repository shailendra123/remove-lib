import { NgModule } from '@angular/core';
import { PackComponent } from './pack.component';



@NgModule({
  declarations: [
    PackComponent
  ],
  imports: [
  ],
  exports: [
    PackComponent
  ]
})
export class PackModule { }
